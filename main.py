#!/usr/bin/env python3
# coding=utf-8

import asyncio
import logging
import os
import sys
import subprocess

print("=== Проверка зависимостей ===")
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
pip_command = [sys.executable, "-m", "pip", "install", "--no-deps",
               "-r", SCRIPT_DIR + "/requirements.txt"]
if sys.version_info.minor < 7:  # there's no idna-ssl for python 3.7
    pip_command.append("idna-ssl==1.0.1")
subprocess.check_call(pip_command)

from listen import Account

logging.basicConfig(format="%(asctime)s| %(levelname)s: %(message)s",
                    datefmt="%H:%M:%S", level=logging.INFO)
logging.getLogger("asyncio").setLevel(logging.WARNING)

with open(SCRIPT_DIR + "/README.txt") as readme:
    print(readme.read())

tokens_filename = SCRIPT_DIR + "/tokens.txt"
open(tokens_filename, 'a').close()  # create file if not exists

with open(tokens_filename) as tokens_file:
    tokens = [token.strip() for token in tokens_file if token.strip()]

for token in tokens:
    account = Account(token)
    asyncio.ensure_future(account.mark_messages_important())
    logging.info("%s:аккаунт добавлен", account.short_token)

loop: asyncio.AbstractEventLoop = asyncio.get_event_loop()
loop.run_forever()
